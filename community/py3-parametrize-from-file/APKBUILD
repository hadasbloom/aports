# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=py3-parametrize-from-file
pkgver=0.10.0
pkgrel=0
pkgdesc="Parametrize From File"
url="https://parametrize-from-file.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	py3-toml
	py3-yaml
	py3-nestedtext
	py3-tidyexc
	py3-more-itertools
	py3-contextlib2
	py3-decopatch
	py3-voluptuous
	"
makedepends="
	py3-build
	py3-flit
	py3-pip
	"
checkdepends="
	py3-pytest
	py3-voluptuous
	py3-numpy
	"
source="https://github.com/kalekundert/parametrize_from_file/archive/v$pkgver/parametrize_from_file-v$pkgver.tar.gz"
builddir="$srcdir/parametrize_from_file-$pkgver"

build() {
	python3 -m build --skip-dependency-check --no-isolation --wheel .
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest
}

package() {
	local whl=dist/parametrize_from_file-$pkgver-py2.py3-none-any.whl
	pip3 install --no-deps --prefix=/usr --root="$pkgdir" "$whl"
}

sha512sums="
855b3f04bd668f23dd26590bf816620d36df8821d2c78aa4c5865185a5d1399290321cdecb93c568e5452c8ffd517b23ec5fa11c01d9c9f39adb6cefa304e88d  parametrize_from_file-v0.10.0.tar.gz
"
